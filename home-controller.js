import Model from './home-model'

class Controller{
    constructor() {
        this.model = new Model();
    }

    loadMoviesFromBackend() {
        return this.model.loadMoviesFromBackend()
    }

}

export default Controller;