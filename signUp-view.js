import Controller from "./signUp-controller";

const controller = new Controller();

const form = document.getElementById('contact-details')
const firstName = document.getElementById('firstName')
const lastName = document.getElementById('lastName')
const userName = document.getElementById('userName')
const passwordTest = document.getElementById('passwordTest')
const password = document.getElementById('password')
const email = document.getElementById('email')
const number = document.getElementById('number')
const button = document.getElementById('button')

    form.addEventListener('submit', (e) => {
    e.preventDefault();
    controller.setContacts(firstName.value, lastName.value, userName.value, passwordTest.value,
                             password.value, email.value, number.value)

    .then( (res) =>{
        console.log(res)
    })
    .catch( (err) => {
        console.log(err)
    })
    form.reset()

    window.location.href ='./tickets.html'

})