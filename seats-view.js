import Controller from './seats-controller.js';

const controller = new Controller();

// const loading = document.getElementById('loading')

const rootDiv = document.getElementById('root')

// function initSeats() {
//     controller.loadSeatsFromBackend()
//         .then(result => {
//             // loading.classList.add('hidden') b n
//             render1()
//         })
//         .catch(error => {
//             console.log(error)
//         })
// }

function renderRows() {

    var rows = controller.getRows()

    console.log(rows)

    rootDiv.innerHTML = '';

    for (let i=0; i<rows.length; i++) {
        const rowsDiv = document.createElement('div')
        rowsDiv.classList.add('row')

            for(let j=0; j<rows[i].seats.length; j++) {
                const newSeat = document.createElement('div')
                newSeat.classList.add('seat')
                if(rows[i].seats[j]=== 'booked') {
                    newSeat.classList.add('booked')
                }
                if(rows[i].seats[j] === 'selected') {
                    newSeat.classList.add('selected')
                }
                newSeat.addEventListener('click', (e) => {
                    console.log(i, j)
                    controller.selectSeat(i, j)
                    renderRows()
                })
                rowsDiv.append(newSeat)
            }
            rootDiv.append(rowsDiv)
    }


}
renderRows();

// function render1 () {

//     const seats = controller.getSeats()

//     rootDiv.innerHTML = '';

//     for (let i = 0; i <seats.length; i++) {
    
//         const newSeat = document.createElement('div')
//         newSeat.classList.add('seat')
//         newSeat.dataset.seatNumber = i;
    
//         if (seats[i] === 'booked') {
//             newSeat.classList.add('booked')   
//         }
//         if (seats[i] === 'selected') {
//             newSeat.classList.add('selected')   
//         }

//         newSeat.addEventListener('click', (e) => {
//             controller.selectSeat(i)
//             render1()
//         })
//         rootDiv.appendChild(newSeat)
//     }

// }

// // render1()

const button = document.getElementById('book-btn')
    button.addEventListener('click', (e) => {

        const isSeatSelectionSuccess = controller.bookSeats();

        if(isSeatSelectionSuccess){

            window.location.href ='./booking.html'

        }
        else {

            alert('no seats selected')

        }

        // console.log(selectedSeats)

        // window.open('./booking.html')
    })

//     initSeats()


const params = new URLSearchParams(window.location.search)
const screeningId = params.get('screenings')


function initSeats2() {

    console.log(screeningId)

    controller.loadScreenDataFromBackend(screeningId)
    .then(data => {
        renderRows()
        // console.log(data)
        // render2(data.screen.rows)
    })
    .catch(error => {
        console.log(error)
    })
}

initSeats2()

function render2(rows) {

    rootDiv.innerHTML = ''

    console.log(rows)

    for(i=0; i<rows.length; i++) {

        const rowsDiv = document.createElement('div')
        rowsDiv.classList.add('row')
        for(j=0; j<rows[i].noOfSeats; j++) {
                
            const newSeat = document.createElement('div')
            newSeat.classList.add('seat')
            newSeat.dataset.row = rows[i].rowId
            newSeat.dataset.seat = j+1
            // console.log(newSeat)
            rowsDiv.append(newSeat)

            newSeat.addEventListener('click', (e) => {
                console.log(newSeat.dataset.seat, newSeat.dataset.row)
                controller.selectSeat(newSeat.dataset.seat, newSeat.dataset.row)
            })
                
        }
        rootDiv.append(rowsDiv)


    }

}




// render2()

// initSeats2() ---> need to initiate after testing
