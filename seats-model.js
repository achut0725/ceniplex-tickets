import axios from "axios";

class Model {
    constructor() {

        this.rows = []
        // this.rows = [
        //     {
        //         rowId: "A", 
        //         seats: ["available", "booked","available", "available","available", "available","available", "available","available", "available",]
        //     },
        //     {
        //         rowId: "B", 
        //         seats: ["available", "available","available", "available","available", "available","available", "available","available", "available",]
        //     },
        //     {
        //         rowId: "C", 
        //         seats: ["available","available", "available","available", "available","available", "available","available", "available",]
        //     },
        //     {
        //         rowId: "D", 
        //         seats: ["available", "available","available", "available","available", "available","available", "available",]
        //     }
        // ];

    }

    getRows () {
        return this.rows;
    }

    // async loadSeatsFromBackend() {
    //     const res = await axios.get('http://localhost:9000/seats')
    //     const seats = res.data
    //     this.seats = seats.map(seat => {
    //         if(seat === true) {
    //             return 'booked'
    //         }else{
    //             return 'available'
    //         }
    //     })
    //     return true;
    // }

    async loadScreenDataFromBackend(id) {
        
        const res = await axios.get('http://localhost:9000/screenings/'+id)
        const screenings = res.data
        console.log(screenings)

        const bookedRes = await axios.get('http://localhost:9000/booking/booked')
        const bookedSeats = (bookedRes.data)
        console.log(bookedSeats)

        this.rows = screenings.screen.rows.map(row => {
            let newRow = 
                {
                    rowId: row.rowId,
                    seats: []
                }

                for(let i=0; i<row.noOfSeats; i++) {

                    const bookedSeat = bookedSeats.filter(bkSeat => {
                        // console.log(bkSeat)
                        if(bkSeat.rowId === row.rowId && bkSeat.seat === i+1){
                            return bkSeat
                        }
                    })
                    if(bookedSeat.length === 0) {
                    newRow.seats.push("available")

                    }
                    else{
                    newRow.seats.push("booked")

                    }

                }
 
                // console.log(newRow)
            
            return newRow

            
        })

        // this.seats.push(selected)

        // console.log(seats)

        return screenings;

    }

    // seatsDataFromSceenData(screenings) {

    //     const seatsData = {
    //         rows : screenings.screen.rows,
    //         seats : screenings.screen.rows[i].noOfSeats
    //     }

    //     console.log(seatsData)

    // }

    getSeats () {
        return this.seats;
    }

    selectSeat(row, seat) {

        if(this.rows[row].seats[seat] === 'available') {
            this.rows[row].seats[seat] = 'selected'
        }
        else if(this.rows[row].seats[seat] === 'selected') {
            this.rows[row].seats[seat] = 'available'
        }

        // console.log(this.seats[i])

        // if (this.seats[i] === 'available') {
        //     this.seats[i] = 'selected'
        //     console.log("selected")

        // }else if(this.seats[i] === 'selected'){
        //     this.seats[i] = 'available'
        //     console.log("available")
        // }

        // // let seatsSelected = []

        // this.seats.push(selected)

        // console.log(this.seats)

    }
    
    bookSeats() {

        let selectedSeats = []

        for(let rowIndex=0; rowIndex<this.rows.length; rowIndex++) {
            for(let seatIndex=0; seatIndex<this.rows[rowIndex].seats.length; seatIndex++) {
                if(this.rows[rowIndex].seats[seatIndex] === 'selected') {
                    const selectedSeat = {
                        rowId: this.rows[rowIndex].rowId,
                        seat: seatIndex+1
                    }
                    selectedSeats.push(selectedSeat)
                }
            }
        }

        console.log(selectedSeats)

        // this.seats.forEach((seat, i) => {
        //     if (seat === 'selected') {
                
        //         selectedSeats.push(i)

        //         // console.log(selectedSeats)
                 
        //     }
        // })

        if(selectedSeats.length === 0){
            return false
        }
        else {
            localStorage.setItem('seats', JSON.stringify(selectedSeats))
            return true
        }

    }
}

export default Model;



// npm i axios
