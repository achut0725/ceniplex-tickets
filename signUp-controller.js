import Model from "./booking-model";

class Controller {
    constructor() {

        this.model = new Model();

    }

    getContacts() {
        return this.model.getContacts();
    }

    setContacts(firstName, lastName, userName, passwordTest, password, email, number) {

        let isvalid = true

        if (firstName.length <= 0 || firstName.length >= 15  )  {
            isvalid = false
            console.log("please fill this column")
            return
        }
        if (lastName.length <= 0 || lastName.length >= 15  )  {
            isvalid = false
            console.log("please fill this column")
            return
        }
        if (userName.length <= 0 || userName.length >= 15  )  {
            isvalid = false
            console.log("please fill this column")
            return
        }
        
        if (number.length < 10 || number.length > 12 ) {
            isvalid = false
            console.log("enterv a valid contact number")
            return
        }

        const seatsSelected = JSON.parse(localStorage.getItem('seats'))
        console.log(seatsSelected)
        

        let values = {
            name : name,
            email : email,
            number : number,
            seatsSelected : seatsSelected
        }

        return this.model.setContacts(values)

    }
}

export default Controller;