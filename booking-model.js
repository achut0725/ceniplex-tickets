import axios from "axios";
import QRCode from 'qrcode'

const axios = require('axios');

class Model {
    constructor() {

        this.contacts = []

    }
    async genarateQrCode(ticket) {

        const qr = await QRCode.toDataURL(ticket._id)
        return qr
    }

    async loadTicketsFromBackend() {
        const res = await axios.get('http://localhost:9000/booking')

        const bookingsWithQr = []
        for(let i=0; i<res.data.length; i++){
            const qr = await this.genarateQrCode(res.data[i])
            res.data[i]['qr']=qr

            bookingsWithQr.push(res.data[i])
        }

        // const booking = res.data 

        // const bookedRes = await axios.get('http://localhost:9000/booking/booked')
        // console.log(bookedRes)

        return bookingsWithQr;
    }

    async getContacts() {
        // return this.contacts;
        const res = await axios.get('http://localhost:9000/booking');

        return res.data;

    }

    async setContacts(contact) {

        console.log(contact)
        const res = await axios.post('http://localhost:9000/booking', contact)
        // this.contacts.push(contact)
        return res 
        
    }

}

export default Model;