import Controller from "./movie-controller";

const controller = new Controller();

const rootDiv = document.getElementById('root')
const image = document.getElementById('image')
const movieName = document.getElementById('movieName')
const duration = document.getElementById('duration')
const summery = document.getElementById('summery')

const params = new URLSearchParams(window.location.search)
const movieId = params.get('id')

function initMovies() {
    controller.loadMovieFromBackend(movieId)
    .then(data => {
        console.log(data)
        render1(data)
    })
    .catch(error => {
        console.log(error)
    }) 
}

function render1 (movieDetails) {

    rootDiv.innerHTML = ''

        const movie = document.createElement('div')

        const image = document.createElement('img')
        image.src = movieDetails.image
        image.classList.add('image')
        movie.append(image)

        const movieName = document.createElement('p')
        movieName.innerHTML = movieDetails.movieName
        movieName.classList.add('movieName')
        movie.append(movieName)

        const discription = document.createElement('p')
        discription.innerHTML = movieDetails.discription
        discription.classList.add('discription')
        movie.append(discription)

        const duration = document.createElement('p')
        duration.innerHTML = movieDetails.duration
        duration.classList.add('duration')
        movie.append(duration)

        const summery = document.createElement('p')
        summery.innerHTML = movieDetails.summery
        summery.classList.add('summery')
        movie.append(summery)

        rootDiv.append(movie)

    }

initMovies()



const screeningsDiv = document.getElementById('root2')
const screenNumber = document.getElementById('screenNumber')
const time = document.getElementById('time')


function initScreening() {
    controller.loadScreeningsFromBackend(movieId)
    .then(data => {
        console.log(data)
        render2(data)
    })
    .catch(error => {
        console.log(error)
    })
     
}

function render2 (screening) {

    screeningsDiv.innerHTML = ''

    for(i=0; i<screening.length; i++) {

            console.log(screening[i])

        const screenings = document.createElement('div')
        screenings.dataset.id=screening[i]._id
        screenings.classList.add('screenings')
        
        const bookLink = document.createElement('a')
        bookLink.innerText = 'Book Seats'
        bookLink.href = './seats.html?screenings='+screenings.dataset.id
        bookLink.classList.add('bookLink')
        screenings.append(bookLink)

        const screenNumber = document.createElement('p')
        screenNumber.innerHTML = `Screen - ${screening[i].screen.screenNumber}`
        screenNumber.classList.add('screenNumber')
        screenings.append(screenNumber)

        const time = document.createElement('p')
        time.innerHTML = `Show starts at - ${screening[i].time}`
        time.classList.add('time')
        screenings.append(time)

        screeningsDiv.append(screenings)

    }

}

initScreening()

// 