import Controller from "./tickets-controller.js";
const controller = new Controller();

const rootDiv = document.getElementById('root')
const name = document.getElementById('name')
const email = document.getElementById('email')
const number = document.getElementById('number')

function initTickets() {
    controller.loadTicketsFromBackend()
    .then(data => {
        render(data)
    })
    .catch(error => {
        console.log(error)
    })
}

function render (booking) {

    rootDiv.innerHTML = ''

    for(i=0; i<booking.length; i++) {



        const tickets = document.createElement('div')
        tickets.classList.add('tickets')

        const qrCode = document.createElement('img')
        qrCode.src = booking[i].qr
        qrCode.classList.add('qr')
        tickets.append(qrCode)

        const ticketsDetaiis = document.createElement('div')
        ticketsDetaiis.classList.add('ticket-details')
        tickets.append(ticketsDetaiis)

        const name = document.createElement('p')
        name.innerHTML = `Name : ${booking[i].name}`  //template literals
        ticketsDetaiis.append(name)

        const email = document.createElement('p')
        email.innerHTML = `Email id : ${booking[i].email}`
        ticketsDetaiis.append(email)

        const number = document.createElement('p')
        number.innerHTML = `Phone number : ${booking[i].number}`
        ticketsDetaiis.append(number)

        const seatsSelected = document.createElement('p')

        let seatsString = ''

        for(let seatCounter = 0; seatCounter<booking[i].seatsSelected.length; seatCounter++) {

            let seatString = booking[i].seatsSelected[seatCounter].rowId+booking[i].seatsSelected[seatCounter].seat
            seatsString = seatsString+ (seatCounter===0? '':', ') +seatString


        }
        seatsSelected.innerHTML = `Selected Seats : ${seatsString}`

        
        // console.log(booking[i].seatsSelected)
        ticketsDetaiis.append(seatsSelected)

        const button = document.createElement('button')
        button.innerHTML = 'REMOVE'
        button.classList.add('remove-button')
        ticketsDetaiis.append(button)

        rootDiv.append(tickets)

    }

}

initTickets();