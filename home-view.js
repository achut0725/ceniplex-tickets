import Controller from "./home-controller";

const controller = new Controller();

const rootDiv = document.getElementById('root')
const image = document.getElementById('image')
const movieName = document.getElementById('movieName')
const discription = document.getElementById('discription')


function initMovies() {
    controller.loadMoviesFromBackend()
    .then(data => {
        console.log(data)
        render(data)
    })
    .catch(error => {
        console.log(error)
    }) 
}

function render (moviesList) {

    rootDiv.innerHTML = ''

    for(i=0; i<moviesList.length; i++) {

        const movies = document.createElement('div')
        movies.dataset.id=moviesList[i]._id
        movies.classList.add('movies')

        const moviesDetails = document.createElement('div')
        moviesDetails.classList.add('movies-details')
        movies.append(moviesDetails)

        const image = document.createElement('img')
        image.src = moviesList[i].image
        moviesDetails.append(image)
        

        const movieName = document.createElement('p')
        movieName.innerHTML = moviesList[i].movieName
        movieName.classList.add('movieName')
        moviesDetails.append(movieName)

        const discription = document.createElement('p')
        discription.innerHTML = moviesList[i].discription
        discription.classList.add('discription')
        moviesDetails.append(discription)

        movies.addEventListener('click', (e) => {
            
            window.location.href = './movie.html?id='+movies.dataset.id

        })

        rootDiv.append(movies)

    }

}


initMovies()