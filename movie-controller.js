import Model from './movie-model'

class Controller{
    constructor() {
        this.model = new Model();
    }

    loadMovieFromBackend(movieId) {
        return this.model.loadMovieFromBackend(movieId)
    }

    loadScreeningsFromBackend(movieId) {
        return this.model.loadScreeningsFromBackend(movieId)
    }

}

export default Controller;




// I'm getting the id of the specific element from the mongodb,but i need help in opening a page using that specific id.Please help me with this model.js