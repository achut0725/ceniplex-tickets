import Model from './seats-model.js'

class Controller {

    constructor() {

        this.model = new Model();

    }

    loadSeatsFromBackend() {
        return this.model.loadSeatsFromBackend()
    }

    getSeats() {
        return this.model.getSeats();
    }

    getRows() {
        return this.model.getRows();
    }

    loadScreenDataFromBackend(screeningId) {
        return this.model.loadScreenDataFromBackend(screeningId)
    }

    selectSeat(row, seat){

        return this.model.selectSeat(row, seat)
    }

    bookSeats() {
        return this.model.bookSeats()
    }

}

export default Controller;